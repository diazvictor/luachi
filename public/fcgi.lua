#!/usr/bin/env lua
--[[
          _                     _     _
         | |   _   _  __ _  ___| |__ (_)
         | |  | | | |/ _` |/ __| '_ \| |
         | |__| |_| | (_| | (__| | | | |
         |_____\__,_|\__,_|\___|_| |_|_|
Copyright (c) 2020  Díaz  Víctor  aka  (Máster Vitronic)
<vitronic2@gmail.com>   <mastervitronic@vitronic.com.ve>
]]--

package.path	=	package.path .. ";../?.lua;../libraries/?.lua;../vendor/?.lua";
fcgi 		= 	require( "fcgi" )
local posix	=	require('posix')
ENV = posix.getenv()
require('luachi')
local format	=	string.format

while fcgi.accept() do
    --fcgi.print( "Set-Cookie: Luachi=a10546b98d3dda15be95194e7d42cfe6;\n" )
    --fcgi.print("Content-type: text/html; charset=utf-8\r");
	local route = fcgi.getenv( "REQUEST_URI" )
	if ( route ~= nil ) then
		local file,controller = nil
		--incluyo el modelo
		file=common:isfile(format('%s/../models/%s.lua',root,route))
		if (file) then
			model = require(format('models.%s',route));
		end
		--incluyo el controlador
		file=common:isfile(format('%s/../controllers/%s.lua',root,route))
		if (file) then
			controller = require(format('controllers.%s',route));
			controller:execute();
		else
			controller = require("controllers.404");
			controller:execute();
		end
	else
		--El modulo por default
		model 	   = require("models.start");
		controller = require("controllers.start");
		controller:execute();
	end
    --fcgi.dumpenv()
	--fcgi.print(os.getenv())
	--for i, s in pairs(posix.getenv()) do
		--print(i,s)
	--end
end

--SERVER_NAME	fcgi
--SCRIPT_NAME	/dispatch.fcgi.lua
--QUERY_STRING	handler=start
--FCGI_ROLE	RESPONDER
--SERVER_ADDR	0.0.0.0
--GATEWAY_INTERFACE	CGI/1.1
--REMOTE_ADDR	127.0.0.1
--SERVER_PORT	80
--HTTP_UPGRADE_INSECURE_REQUESTS	1
--SCRIPT_FILENAME	/home/vitronic/Proyectos/Luachi-ng/public/dispatch.fcgi.lua
--REQUEST_URI	/start
--SERVER_PROTOCOL	HTTP/1.1
--HTTPS	off
--HTTP_RETURN_CODE	200
--HTTP_CACHE_CONTROL	no-cache
--HTTP_PRAGMA	no-cache
--HTTP_SCHEME	http
--REDIRECT_STATUS	200
--HTTP_CONNECTION	keep-alive
--HTTP_DNT	1
--SERVER_SOFTWARE	Hiawatha v10.9
--HTTP_COOKIE	Luachi=a10546b98d3dda15be95194e7d42cfe6
--DOCUMENT_ROOT	/home/vitronic/Proyectos/Luachi-ng/public
--HTTP_ACCEPT_ENCODING	gzip, deflate
--HTTP_ACCEPT_LANGUAGE	es-VE,en-US;q=0.7,en;q=0.3
--HTTP_ACCEPT	text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
--HTTP_USER_AGENT	Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0
--HTTP_HOST	fcgi
--REQUEST_METHOD	GET
