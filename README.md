# About Luachi

Luachi is built with the Model View Controller (MVC) paradigm so the separation of the programming logic and the graphical interface allows for clean development, fast and powerful deployments of your web application.

## Features

- HTTP Requests support.
- Route Parameters support.
- Logging support.
- Database support for SQLite y PostgreSQL.
- Session support.
- Localization support.

## Dependencies

- [lua-crypt](https://github.com/jprjr/lua-crypt.git) (to encrypt passwords)
- [luapgsql](https://github.com/arcapos/luapgsql) (to support the PostgreSQL database)
- [luasqlite3](http://lua.sqlite.org/index.cgi/home) (to support SQLite database)
- [lua-imagick](https://github.com/isage/lua-imagick) (for captcha generation)

**FastCGI supported**

- [spawn-fcgi](https://redmine.lighttpd.net/projects/spawn-fcgi)
- [fcgiwrap](https://github.com/gnosek/fcgiwrap)

**To run with fastcgi**

`spawn-fcgi -n -u nobody -g nobody -M 0775 -F 10 -s /run/fcgiwrap.sock -U nobody -G nobody /usr/bin/fcgiwrap`

`sudo -u nobody spawn-fcgi -n -u nobody -g nobody -M 0775 -F 10 -s /run/fcgiwrap.sock -U nobody -G nobody /usr/bin/fcgiwrap`

then configure hiawatha to listen to that socket

## LICENSE

The Luachi Framework is open-sourced software under the [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) license.
